'use strict'

angular.module('student-fly.book-editing')
# config route foreach controller
.config ($routeProvider) ->
	$routeProvider.when '/bespoke-cover',
		controller: 'bespoke-cover-ctrl'
		templateUrl: 'views/book-editing/bespoke-cover.jade'

.controller 'bespoke-cover-ctrl', ($scope, $location) ->
	$scope.title = 'bespoke-cover'
