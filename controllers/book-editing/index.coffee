'use strict'

angular.module('student-fly.book-editing', [])
# config route foreach controller
.config ($routeProvider) ->
	$routeProvider.when '/book-editing',
		controller: 'book-editing-ctrl'
		templateUrl: 'views/book-editing/index.jade'

.controller 'book-editing-ctrl', ($scope, $location) ->
	$scope.title = 'Book Editing'
	$scope.template = 'views/book-editing/customized-cover.jade'

	$scope.change = (template) ->
		# alert template
		$scope.template = 'views/book-editing/' + template

