'use strict'

angular.module('student-fly.book-editing')
# config route foreach controller
.config ($routeProvider) ->
	$routeProvider.when '/customized-cover',
		controller: 'customized-covering-ctrl'
		templateUrl: 'views/book-editing/customized-cover.jade'

.controller 'customized-cover-ctrl', ($scope, $location) ->
	$scope.title = 'customized-cover'
