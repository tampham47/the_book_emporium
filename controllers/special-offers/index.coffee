'use strict'

angular.module('student-fly.special-offers', [])
# config route foreach controller
.config ($routeProvider) ->
	$routeProvider.when '/special-offers',
		controller: 'special-offers-ctrl'
		templateUrl: 'views/special-offers/index.jade'

.controller 'special-offers-ctrl', ($scope, $location) ->
	$scope.title = 'Special Offers'
