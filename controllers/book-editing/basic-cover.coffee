'use strict'

angular.module('student-fly.book-editing')
# config route foreach controller
.config ($routeProvider) ->
	$routeProvider.when '/basic-cover',
		controller: 'basic-cover-ctrl'
		templateUrl: 'views/book-editing/basic-cover.jade'

.controller 'basic-cover-ctrl', ($scope, $location) ->
	$scope.title = 'basic-cover'

	$scope.ok = ->
		alert 'basic-cover-ctrl'
