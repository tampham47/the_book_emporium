'use strict'

angular.module('student-fly.basket', [])
# config route foreach controller
.config ($routeProvider) ->
	$routeProvider.when '/basket',
		controller: 'basket-ctrl'
		templateUrl: 'views/basket/index.jade'

.controller 'basket-ctrl', ($scope, $location) ->
	$scope.title = 'Basket'
