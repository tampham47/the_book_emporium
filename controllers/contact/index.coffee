'use strict'

angular.module('student-fly.contact', [])
# config route foreach controller
.config ($routeProvider) ->
	$routeProvider.when '/contact',
		controller: 'contact-ctrl'
		templateUrl: 'views/contact/index.jade'

.controller 'contact-ctrl', ($scope, $location) ->
	$scope.title = 'Contact Us'
