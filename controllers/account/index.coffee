'use strict'

angular.module('student-fly.account', [])
# config route foreach controller
.config ($routeProvider) ->
	$routeProvider.when '/account',
		controller: 'account-ctrl'
		templateUrl: 'views/account/index.jade'

.controller 'account-ctrl', ($scope, $location) ->
	$scope.title = 'Account'
