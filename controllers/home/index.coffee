'use strict'

angular.module('student-fly.home', [])
# config route foreach controller
.config ($routeProvider) ->
	$routeProvider.when '/',
		controller: 'home-ctrl'
		templateUrl: 'views/home/index.jade'

.controller 'home-ctrl', ($scope, $location) ->
	$scope.title = 'Home'
