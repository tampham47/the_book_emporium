'use strict'

angular.module('student-fly.book-publishing', [])
# config route foreach controller
.config ($routeProvider) ->
	$routeProvider.when '/book-publishing',
		controller: 'book-publishing-ctrl'
		templateUrl: 'views/book-publishing/index.jade'

.controller 'book-publishing-ctrl', ($scope, $location) ->
	$scope.title = 'Book Publishing'
