'use strict'

# require order is important
angular.module 'student-fly', [
	'ngAnimate'
	'ngRoute'

	'student-fly.templates'
	'student-fly.home'
	'student-fly.book-editing'
	'student-fly.book-publishing'
	'student-fly.book-sales'
	'student-fly.contact'
	'student-fly.special-offers'
	'student-fly.account'
	'student-fly.basket'
]
