'use strict'

angular.module('student-fly.book-sales', [])
# config route foreach controller
.config ($routeProvider) ->
	$routeProvider.when '/book-sales',
		controller: 'book-sales-ctrl'
		templateUrl: 'views/book-sales/index.jade'

.controller 'book-sales-ctrl', ($scope, $location) ->
	$scope.title = 'Book Sales'
